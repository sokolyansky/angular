'use strict';

bookApp.controller('BookListCtrl', function($scope, $http) {

    $http.get('data/books.json').success(function(data) {
        $scope.books = data;
    });
    $scope.showCard = function(book) {
        location.href = 'card.html';
    }
});

cardApp.controller('CardBookCtrl', function($scope, $http) {
    $http.get('data/book.json').success(function(data) {
        $scope.book = data;
    })
});